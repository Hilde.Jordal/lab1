// My code is essentially a translation of the python implementation that was included, I asked Sondre about this and 
// understood this was okay as long as it wasn't a complete copy and I tried to code some parts myself.
// I wrote it all by hand while looking up how the things I know from python work in Java,
// I have edited things here and there that needed it and added my own random function. I even tried adding some 
// exception handling but then the codegrade tests failed so I removed them.

package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
		//creates package and runs the run() function
        new RockPaperScissors().run();
    }
    
    // Scanner object and global variables
    Scanner sc = new Scanner(System.in);
    int roundCounter = 0;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> yesnoList = Arrays.asList("y", "n");
    
    // found here: https://stackoverflow.com/questions/8065532/how-to-randomly-pick-an-element-from-an-array 19.01.2022
    public String randomChoice() {
    	Random rand = new Random();
    	String randomElement = rpsChoices.get(rand.nextInt(rpsChoices.size()));
    	return randomElement;
    }
    
    // checks if choice 1 is the winner over choice2
    public boolean isWinner(String choice1, String choice2) {
    	if (choice1.equals("paper")) {
    		return (choice2.equals("rock"));
    	} else if (choice1.equals("scissors")) {
    		return (choice2.equals("paper"));
    	} else {
    		return (choice2.equals("scissors"));
    	}
    }
    
    // Asks user for game input, and also loops if input is incorrect
    public String userChoice() {
    	while (true) {
    		String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
    		if (validateInput(humanChoice, rpsChoices)) {
    			return humanChoice;
    		} else {
    			System.out.println(String.format("I don't understand %s. Try again", humanChoice));
    		}
    	}
    }
    
    // Asks user if the game should continue
    public String continuePlaying() {
    	while (true) {
    		String continueAnswer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
    		if (validateInput(continueAnswer, yesnoList)) { // Yes/no list used because validate input needs a list to check, and those are the accepted responses
    			return continueAnswer;
    		} else {
    			System.out.println(String.format("I do not understand %s. Could you try again?", continueAnswer));
    		}
    	}
    }
    
    // Fixes any capitalization in the user input to fit with string comparison checks
    public boolean validateInput(String input, List<String> validInput) {
    	input = input.toLowerCase();
    	return (validInput.contains(input));
    }
    
    // Game loop
    public void run() {
    	while (true) {
    		roundCounter += 1;
    		System.out.println(String.format("Let's play round %d", roundCounter));
    		String humanChoice = userChoice();
    		String computerChoice = randomChoice();
    		String choiceString = String.format("Human chose %s, computer chose %s.", humanChoice, computerChoice);
    		
    		// Checks who won or else tie, and updates scores
    		if (isWinner(humanChoice,computerChoice)) {
    			System.out.println(choiceString + " Human wins!");
    			humanScore += 1;
    		} else if (isWinner(computerChoice, humanChoice)) {
    			System.out.println(choiceString + " Computer wins!");
    			computerScore += 1;
    		} else {
    			System.out.println(choiceString + " It's a tie!");
    		}
    		System.out.println(String.format("Score: human %d, computer %d", humanScore, computerScore));
    		
    		// Ask to play again
    		String continueAnswer = continuePlaying();
    		if (continueAnswer.equals("n")) {
    			System.out.println("Bye bye :)");
    			break;
    		}
    	}
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
